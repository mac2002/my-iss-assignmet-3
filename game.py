import pygame
import time
import random
import array as arr
import sys
import math

pygame.mixer.pre_init(44100,16,2,4096)
pygame.init()

white = (255, 255, 255)
yellow = (255, 255, 102)
black = (0, 0, 0)
red = (213, 50, 80)
green = (0, 255, 0)
blue = (50, 153, 213)
sand = (210, 123, 58)

dis_width = 600
dis_height = 705

end1 = 588

dis = pygame.display.set_mode((dis_width, dis_height))
dis.fill((50, 153, 213))
pygame.display.flip()
pygame.display.set_caption('River and boat')

clock = pygame.time.Clock()
font_style = pygame.font.SysFont("bahnschrift", 25)
score_font = pygame.font.SysFont("comicsansms", 35)
time_2 = 0

pygame.mixer.music.load("spectre.mp3")
pygame.mixer.music.set_volume(0.5)
pygame.mixer.music.play(-1)
player_img = pygame.image.load('ship.png')
obstacle_img = pygame.image.load('anchor.png')
obstacle_mov_img = pygame.image.load('yatch.png')

def Your_score(score,k,time):
    value = score_font.render("Player:" + str(k) + "Score:" + str(score) , True, white)
    dis.blit(value, [0, 0])

def message(msg, color,x,y):
    mesg = font_style.render(msg, True, color)
    dis.blit(mesg, [x, y])

def player_pos(x,y):
    dis.blit(player_img,(x,y))
def obstacle(x,y):
    dis.blit(obstacle_img,(x,y))
ym = arr.array('d', [105, 146, 257, 319])
obsx = arr.array('d', [0, 568, 0, 568])
def mov_obstacle(x, y):
    dis.blit(obstacle_mov_img, [x, y])

def collide(xp,yp,xo,yo):
    distance = math.sqrt(math.pow((xp - xo), 2) + math.pow((yp-yo), 2))
    if distance < 30:
        return True
    else:
        return False

y = arr.array('i',[86,211,336,461,586])
x = arr.array('d',[])
for i in range (0, 5):
    k = (round(random.randrange(0, 554)/10))*10
    x.append(k)

ref =arr.array('i',[0, 0]) 
index = arr.array('d',[])

ym = arr.array('d', [121, 170, 252, 379])
obsx = arr.array('d', [0, 568, 0, 567])
mobs = arr.array('d', [0.4, -0.4, 0.3, 0.3, -0.3])

def game1Loop(k,x,y,ym,obsx,mobs):
    if k == 1:
        game1_over = False
        game2_over = True
        x1 = 0
        y1 = dis_height - 32
    elif k == 2:
        game1_over = True
        game2_over = False
        x1 = index[0]
        y1 = index[1]
    x1_change=0
    y1_change=0
    x2_change=0
    y2_change=0
    time_1 = 0
    time_2 = 0
    pause = False
    if k == 1:
        while not game1_over:
            while pause:
                dis.fill((255, 255, 255))
                message("GAME PAUSED", black, 240, 200)
                message("Q to QUIT", red, 150, 300)
                message("R to RESUME", green, 350, 300)
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        game1_over = True
                        pause=False
                        break
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_q:
                            pygame.quit()
                            quit()
                        if event.key == pygame.K_r:
                            pause = False
            dis.fill((50, 153, 213))
            pygame.draw.rect(dis, sand, (0, 80, 600, 40))
            pygame.draw.rect(dis, sand, (0, 0, 600, 40))
            pygame.draw.rect(dis, sand, (0, 205, 600, 40)) 
            pygame.draw.rect(dis, sand, (0, 330, 600, 40)) 
            pygame.draw.rect(dis, sand, (0, 455, 600, 40)) 
            pygame.draw.rect(dis, sand, (0, 580, 600, 40))
            pygame.draw.rect(dis, sand, (0, 665, 600, 40))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        y1_change = -0.2
                        x1_change = 0
                    elif event.key == pygame.K_DOWN:
                        y1_change = 0.2
                        x1_change = 0
                    elif event.key == pygame.K_RIGHT:
                        if (x1+0.1) <= 556:
                            x1_change = 0.2
                            y1_change = 0
                    elif event.key == pygame.K_LEFT:
                        if (x1-0.1)>=15:
                            x1_change = -0.2
                            y1_change = 0
                    elif event.key == pygame.K_SPACE:
                        pause = True
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_LEFT:
                        x1_change=0
                        y1_change=0
                    if event.key == pygame.K_RIGHT:
                        x1_change=0
                        y1_change=0
                    if event.key == pygame.K_DOWN:
                        x1_change=0
                        y1_change=0
                    if event.key == pygame.K_UP:
                        x1_change=0
                        y1_change=0

            if y1 < 5:#or time_1 > 30:
                index.append(x1)
                index.append(y1)
                ref[0] = 65
                score_1 = 65
                game1_over = True
            x1+=x1_change
            y1+=y1_change
            if x1 < 0.2:
                x1=0
            if x1 > 567.8:
                x1 = 568
            if y1 > 672.8:
                y1 = 673
            if y1 < 0.2:
                y1 = 0
            player_pos(x1,y1)
            obsx[0] += mobs[0]
            if obsx[0] <= 1:
                mobs[0] = 0.4
            if obsx[0] >= 568:
                mobs[0] = -0.4
            obsx[1] += mobs[1]
            if obsx[1] <= 1:
                mobs[1] = 0.4
            if obsx[1] >= 568:
                mobs[1] = -0.4
            obsx[2] += mobs[2]
            if obsx[2] <= 0:
                mobs[2] = 0.3
            if obsx[2] >= 568:
                mobs[2] = -0.3
            ym[2] += mobs[3]
            if ym[2] <= 252:
                mobs[3] = 0.3
            if ym[2] >=300:
                mobs[3] = -0.3
            obsx[3] += mobs[4]
            if obsx[3] <= 0:
                mobs[4] = 0.3
            if obsx[3] >= 568:
                mobs[4] = -0.3
            for i in range (0, 4):
                mov_obstacle(obsx[i], ym[i])
            for p in range (0, 5):
                obstacle(x[p],y[p])
            for i in range (0, 5):
                hit = collide(x1,y1,x[i],y[i])
                if hit == True:
                    index.append(244)
                    index.append(3)
                    game1_over = True
            for i in range (0, 4):
                hit = collide(x1,y1,obsx[i],ym[i])
                if hit == True:
                    index.append(244)
                    index.append(3)
                    game1_over = True
            score_1 = 0
            for i in range (0, 5):
                if y1 < y[i]:
                    score_1+=5
            for i in range (0, 4):
                if y1 < ym[i]:
                    score_1+=10
            ref[0] = score_1
            Your_score(score_1,1,time_1)
            milli = clock.tick()
            sec=milli/1000
            time_1 += sec
            pygame.display.update()
    if k == 2:
        while not game2_over:
            while pause:
                dis.fill((255, 255, 255))
                message("GAME PAUSED", black, 240, 200)
                message("Q to QUIT", red, 150, 300)
                message("R to RESUME", green, 350, 300)
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        quit()
                    if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_q:
                                game2_over = True
                                pause = False
                            if event.key == pygame.K_r:
                                pause = False
            dis.fill((50, 153, 213))
            pygame.draw.rect(dis, sand, (0, 80, 600, 40))
            pygame.draw.rect(dis, sand, (0, 0, 600, 40))
            pygame.draw.rect(dis, sand, (0, 205, 600, 40)) 
            pygame.draw.rect(dis, sand, (0, 330, 600, 40)) 
            pygame.draw.rect(dis, sand, (0, 455, 600, 40)) 
            pygame.draw.rect(dis, sand, (0, 580, 600, 40))
            pygame.draw.rect(dis, sand, (0, 665, 600, 40))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        y2_change = -0.2
                        x2_change = 0
                    elif event.key == pygame.K_DOWN:
                        y2_change = 0.2
                        x2_change = 0
                    elif event.key == pygame.K_RIGHT:
                        if (x1+0.1) <= 556:
                            x2_change = 0.2
                            y2_change = 0
                    elif event.key == pygame.K_LEFT:
                        if (x1-0.1)>=15:
                            x2_change = -0.2
                            y2_change = 0
                    elif event.key == pygame.K_SPACE:
                        pause = True
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_LEFT:
                        x2_change=0
                        y2_change=0
                    if event.key == pygame.K_RIGHT:
                        x2_change=0
                        y2_change=0
                    if event.key == pygame.K_DOWN:
                        x2_change=0
                        y2_change=0
                    if event.key == pygame.K_UP:
                        x2_change=0
                        y2_change=0
            if y1 >= dis_height-32 or time_2 > 30:
                game2_over = True
                ref[0] = 2
            x1+=x2_change
            y1+=y2_change
            if x1 < 0.2:
                x1=0
            if x1 > 567.8:
                x1 = 568
            if y1 > 687.8:
                y1 = 688
            if y1 < 0.2:
                y1 = 0
            player_pos(x1,y1)
            if obsx[0] <= 0:
                mobs[0] = 0.4
            if obsx[0] >= 568:
                mobs[0] = -0.4
            if obsx[1] <= 0:
                mobs[1] = 0.4
            if obsx[1] >= 568:
                mobs[1] = -0.4
            if obsx[2] <= 0:
                mobs[2] = 0.3
            if obsx[2] >= 567:
                mobs[2] = -0.3
            if ym[2] <= 252:
                mobs[3] = 0.3
            if ym[2] >= 303:
                mobs[3] = -0.3
            if obsx[3] <= 0:
                mobs[4] = 0.3
            if obsx[3] >= 567:
                mobs[4] = -0.3
            obsx[0] += mobs[0]
            obsx[1] += mobs[1]
            obsx[2] += mobs[2]
            ym[2] += mobs[3]
            obsx[3] += mobs[4]
            for i in range (0, 4):
                mov_obstacle(obsx[i], ym[i])
            for p in range (0, 5):
                obstacle(x[p],y[p])
            for i in range (0, 5):
                hit = collide(x1,y1,x[i],y[i])
                if hit == True:
                    game2_over = True
            for i in range (0, 4):
                hit = collide(x1,y1,obsx[i],ym[i])
                if hit == True:
                    game2_over = True
            score_2 = 0
            for i in range (0, 5):
                if y1 > y[i]:
                    score_2+=5
            for i in range (0, 4):
                if y1 > ym[i]:
                    score_2+=10
            ref[1] = score_2
            Your_score(score_2,2,time_2)
            milli = clock.tick()
            sec=milli/1000
            time_2 += sec
            pygame.display.update()

def Game1on():
    start = False
    while not start:
        dis.fill((180,180,180))
        message("Press E to Start The GAME-1!", ((255, 0, 25)), 190, 320)
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_e:
                    start = True
                    break
    game1Loop(1,x,y,ym,obsx,mobs)
    
    close = True
    while close:
        dis.fill((0, 255, 255))
        message("ESC to Quit The Game", red, 190, 270)
        if ref[0] != 65:
            Your_score(ref[0], 1, ref[0])
            message("C to Contiunue", red, 190, 390)
        if ref[0] == 65:
            message("Player 1 Score : 65", red, 190, 330)
            message("N to next Player:", red, 190, 390)
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    quit()
                if event.key == pygame.K_c:
                    close = False
                if event.key == pygame.K_n:
                    close = False
                    break


def Game2on():
    start = False
    while not start:
        dis.fill((180, 180, 180))
        message("Press E to start the Game-2!", ((255, 0, 25)), 190, 320)
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_e:
                    start = True
                    break
    game1Loop(2,x,y,ym,obsx,mobs)

    close = True
    while close:
        dis.fill((0, 255, 255))
        message("ESC to QUIT", red, 190, 290)
        if ref[1] == ref[0]:
            message("TIE", red, 190, 0)
        if ref[1] > ref[0]:
            message("Player 2 WON!", red, 180, 10)
        if ref[1] < ref[0]:
            message("Player 1 WON!", red, 180, 10)
        message("P to Play Again!", red, 190, 370)
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    quit()
                if event.key == pygame.K_p:
                    close = False
                    break


while True:
    Game1on()
    Game2on()
